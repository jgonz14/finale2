@extends('layouts.app')

@section('content')
  <h1>Edit Category</h1>

  {!! Form::model($category, ['route' => ['categories.update', $category->id], 'method' => 'PUT']) !!}
<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    {!! Form::label('name', 'Category Name') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
    <small class="text-danger">{{ $errors->first('name') }}</small>
</div>
<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    {!! Form::label('description', 'Category Description') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
    <small class="text-danger">{{ $errors->first('description') }}</small>
</div>
{!! Form::submit('Update Category', ['class' => 'btn btn-success pull-right']) !!}
{!! Form::close() !!}

@endsection
