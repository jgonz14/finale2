@extends('layouts.app')

@section('content')
  <h1>All Categories</h1>
  @if(Session::has('flash_msg'))
    <div class="alert alert-success">
      {{ Session::get('flash_msg') }}
    </div>

  @endif


      <table class='table table-striped table-bordered table-hover table-condensed'>
    <thead>
      <tr>
        <th>Category Name</th>
          <th>Category Description</th>
            <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($categories as $category)
      <tr>
        <td>{{$category->name}}</td>
        <td>{{$category->description}}</td>
        <td>

          <div class="col-md-2">
               <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-primary">Edit </a>
          </div>

          <div class="col-md-2">
            {!! Form::open([
               'method' => 'DELETE',
               'route' => ['categories.destroy', $category->id]
               ]) !!}
               {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
           {!! Form::close() !!}
          </div>





        </td>
      </tr>
      @endforeach
    </tbody>
  </table>

    {{$categories->render()}}

  <div class="pull-right">

    <a href="/categories/create" class="btn btn-primary">Create Category</a>
  </div>

@endsection
