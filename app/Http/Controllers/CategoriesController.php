<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Category;
use Session;

class CategoriesController extends Controller
{
  public function  __construct(){
    $this->middleware('auth');
  }
  public function index(){
   $categories = Category::paginate(5);
   return view('categories.index',compact('categories'));
 }

 public function create(){
   return view('categories.create');
 }

 public function edit($id){
   //dd($id);
   $category = Category::findOrfail($id);
   return view('categories.edit',compact('category'));
 }

 public function store(Request $request){
   $this->validate($request,[
     'name' => 'required|min:5|max:10|unique:categories,name',
     'description' => 'required',
   ]);

   $input = $request->all();
   Category::create($input)->save();
   Session::Flash('flash_msg','Successfully Created Category');

   return redirect('/categories');
   //dd($input);-to display
 }
 public function update($id,Request $request){
   $this->validate($request,[
   'name' => 'required|min:5|max:10',
   'description' => 'required',
 ]);
   $input = $request->all();

   $category = Category::findOrfail($id);

   $category->fill($input)->save();

   Session::Flash('flash_msg','Successfully Updated Category');

   return redirect('/categories');
 }

 public function destroy($id){
 $category = Category::findOrFail($id);
 $category->delete();
 Session::flash('flash_msg','Successfully Deleted Category');
 return redirect('/categories');
}
}
