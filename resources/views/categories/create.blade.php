@extends('layouts.app')

@section('content')
  <h1>Create Category</h1>

  {!! Form::open(['method' => 'POST', 'route' => 'categories.store', 'class' => 'form-horizontal']) !!}

    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label('name', 'Category Name') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('name') }}</small>
    </div>

    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
        {!! Form::label('description', 'Category Description') !!}
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('description') }}</small>
    </div>

    {!! Form::submit('Create Category', ['class' => 'btn btn-success pull-right']) !!}
    {!! Form::close() !!}

@endsection
