<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

// http://localhost:8000/categories
Route::get('/categories', 'CategoriesController@index');
// http://localhost:8000/categories/create
Route::get('/categories/create', 'CategoriesController@create');
// http://localhost:8000/categories/edit
Route::get('/categories/edit', 'CategoriesController@edit');
// http://localhost:8000/categories/store
Route::post('/categories/store',[ //create
  'as' => 'categories.store',
  'uses' => 'CategoriesController@store'
]);

Route::get('/categories/edit/{id}',[ //fetch
  'as' => 'categories.edit',
  'uses' => 'CategoriesController@edit'
]);
Route::put('/categories/update/{id}',[ //update
  'as' => 'categories.update',
  'uses' => 'CategoriesController@update'
]);

Route::delete('/categories/destroy/{id}',[
  'as' => 'categories.destroy',
  'uses' => 'CategoriesController@destroy'
]);
